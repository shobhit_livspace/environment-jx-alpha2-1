CONFIG = {
    "DJANGO_LOG_LEVEL": "INFO",
    "DATABASE": {
        "HOST": "launchpad-postgres",
        "USERNAME": "postgres",
        "PASSWORD": "livspaceadmin",
        "DB_NAME": "odin-alpha2",
        "PORT": "5432",
    },
    "DEBUG": False,
    "ALLOWED_HOSTS": ["*"],
    "EVENT_SERVICE": {
        "environment": "local",
        "system_name": "odin"
    },
    "SENTRY": {
        "ENABLED": True,
        "ENVIRONMENT": "alpha2",
        "DSN": "https://eda272d29c0540d896fe3a9edd7e1573@sentry.livspace.com/77"
    }
}

GATEWAY = {
    "HOST": "api.alpha2.livspace.com",
    "HEADERS": {
        "Authorization": "Basic U3Rhck1TRmUtRzREc0tJOjJTWGhMeEc3cHJhYmtXbDJySFFwdGJwSXl0OFhIOXln",
        "Content-Type": "application/json"
    }
}

EXTERNAL_SERVICES = {
    "LAUNCHPAD": {
        'HOST': 'api.alpha2.livspace.com/launchpad-backend',
        'HEADERS': {
            "Authorization": "Basic U3Rhck1TRmUtRzREc0tJOjJTWGhMeEc3cHJhYmtXbDJySFFwdGJwSXl0OFhIOXln",
            "X-Requested-By": "0",
            "Content-Type": "application/json"
        },
        "GATEWAY": {
            "ENABLED": True,
            "PATH": '/launchpad-backend'
        }
    },
    'BOUNCER': {
        'HOST': 'api.alpha2.livspace.com/bouncer',
        'HEADERS': {
            "Authorization": "Basic U3Rhck1TRmUtRzREc0tJOjJTWGhMeEc3cHJhYmtXbDJySFFwdGJwSXl0OFhIOXln",
            "X-Requested-By": "0",
            "Content-Type": "application/json"
        },
        "GATEWAY": {
            "ENABLED": True,
            "PATH": '/bouncer'
        }
    },
    'ENIGMA': {
        'HOST': 'api.alpha2.livspace.com/enigma',
        'HEADERS': {
            "Authorization": "Basic U3Rhck1TRmUtRzREc0tJOjJTWGhMeEc3cHJhYmtXbDJySFFwdGJwSXl0OFhIOXln",
            "X-Requested-By": "0",
            "Content-Type": "application/json",
            "auth-override-token": "zwOh6fJQN5vZTnH9CyjyJETBAWRBaFzQ"
        },
        "GATEWAY": {
            "ENABLED": False,
            "PATH": '/enigma'
        }
    },
    'FMS': {
        'HOST': 'fms.alpha2.livspace.com',
        'HEADERS': {
            "Authorization": "Basic U3Rhck1TRmUtRzREc0tJOjJTWGhMeEc3cHJhYmtXbDJySFFwdGJwSXl0OFhIOXln",
            "X-Requested-By": "0",
            "Content-Type": "application/json",
        },
        "GATEWAY": {
            "ENABLED": True,
            "PATH": '/fms'
        }
    },
    'BOQ': {
        'HOST': 'api.alpha2.livspace.com/backoffice',
        'HEADERS': {
            "Authorization": "Basic U3Rhck1TRmUtRzREc0tJOjJTWGhMeEc3cHJhYmtXbDJySFFwdGJwSXl0OFhIOXln",
            "X-Requested-By": "0",
            "Content-Type": "application/json",
            "X-Auth-Token": "8cQ904440221g0kWq8wE7N68T48DcfMr"
        },
        'GATEWAY': {
            "ENABLED": False,
            "PATH": '/backoffice'
        }
    },
    'CMS': {
        'HOST': 'api.alpha2.livspace.com/seeker',
        'HEADERS': {
            "Authorization": "Basic U3Rhck1TRmUtRzREc0tJOjJTWGhMeEc3cHJhYmtXbDJySFFwdGJwSXl0OFhIOXln",
            "X-Requested-By": "0",
            "Content-Type": "application/json",
        },
        'GATEWAY': {
            "ENABLED": True,
            "PATH": '/seeker'
        }
    },
    'CARBON': {
        'HOST': 'api.alpha2.livspace.com/carbon',
        'HEADERS': {
            "X-Auth-Token": "VQmcwb8rlxRAr2KqpAXYo1XZ8ZCscDqh",
            "X-Requested-By": "Launchpad",
            "X-Request-Id": "4e98c0bc-b4d7-4c9a-ae87-7ff1a9a2fd3f"
        },
        "GATEWAY": {
            "ENABLED": True,
            "PATH": '/carbon'
        }
    }
}