CONFIG = {
    "DJANGO_LOG_LEVEL": "INFO",
    "DEBUG": True,
    "ALLOWED_HOSTS": ["*"],
    "EVENT_SERVICE": {
        "environment": "local",
        "system_name": "skurge"
    },
    "SENTRY": {
        "ENABLED": True,
        "ENVIRONMENT": "alpha2",
        "DSN": "https://a2bb2e7b74fc42659d4cb5578da91f39@sentry.livspace.com/74"
    }
}

EXTERNAL_SERVICES = {
    "LAUNCHPAD": {
        "HOST": "launchpad-backend",
        "HEADERS": {
            "Content-Type": "application/json"
        },
        "GATEWAY": {
            "ENABLED": False
        }
    },
    "CALENDAR": {
        "HOST": "calendar",
        "HEADERS": {
            "X-Requested-By": "Launchpad",
            "X-Request-Id": "4e98c0bc-b4d7-4c9a-ae87-7ff1a9a2fd3f",
            "Content-Type": "application/json"
        },
        "GATEWAY": {
            "ENABLED": False
        }
    },
    "DAAKIYA": {
        "HOST": "daakiya",
        "HEADERS": {
            "Content-Type": "application/json"
        },
        "GATEWAY": {
            "ENABLED": False
        }
    },
    "BOUNCER": {
        "HOST": "bouncer",
        "HEADERS": {
            "X-CLIENT-ID": "LAUNCHPAD",
            "X-CLIENT-SECRET": "6ee38134-64c0-4095-af8a-38f05e69b0bd"
        },
        "GATEWAY": {
            "ENABLED": False
        }
    }
}